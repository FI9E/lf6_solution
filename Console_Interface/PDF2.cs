﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console_Interface
{
    class PDF2
    {
        private static short menuInput;

        private static bool parsed;

        private static string[] MenuPointsPDF = {
            "Zurück",
            "Aufgabe a) BMI Rechner",
            "Aufgabe b) C# Funktionen"
        };

        public static void Menu()
        {
            do
            {
                Console.Clear();
                AsciiArt.FileTitle("PDF 2 - Menü");
                AsciiArt.Menu(MenuPointsPDF);
                menuInput = AsciiArt.MenuInput(parsed, MenuPointsPDF);

                switch (menuInput)
                {
                    case 1:
                        BMIRechner();
                        AsciiArt.EndOfCodeWarning();
                        break;
                    case 2:
                        CSharpFunktionen();
                        AsciiArt.EndOfCodeWarning();
                        break;
                    case 0:
                        return;
                    default: Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Error: unable to use Menu-Switch!"); Console.ForegroundColor = ConsoleColor.White; break;

                }
            } while (menuInput != 0);

            menuInput = -1;
            return;
        }



        private static void BMIRechner()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 2 - Anwendung > BMI Rechner ");

            Console.Write("\nKörpergröße: ");
            double körpergröße = Convert.ToDouble(Console.ReadLine());

            Console.Write("\nGewicht: ");
            double gewicht = Convert.ToDouble(Console.ReadLine());

            double bmiKörpergröße = körpergröße * körpergröße;

            double bmi = gewicht / bmiKörpergröße;
            bmi = bmi * 10000;
            Console.WriteLine("\nDein Body Mass Index ist : " + bmi);
        }

        private static void CSharpFunktionen()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 2 - Lösung > C# Funktionen ");

            Console.WriteLine("\n\n\nFrage 1:");
            Console.WriteLine("Wie wird eine Funktion definiert?");

            Console.WriteLine("\nAntwort: ");
            Console.WriteLine("- Eine Funktion ist ein Wiederaufrufbarer Teil des Quellcodes, dem Parameter übergeben werden können und daten zurückgeben kann.");

            Console.WriteLine("\n\nFrage 2:");
            Console.WriteLine("Was sind Übergabe-Parameter (Funktionsparameter)?");

            Console.WriteLine("\nAntwort: ");
            Console.WriteLine("- Funktionsparameter werden von FUnktionen zu Funktionen übergeben und können datentypen, objekte und sogar funktionen beinhalten.");

            Console.WriteLine("\n\nFrage 3:");
            Console.WriteLine("Was ist der Rückgabewert?");

            Console.WriteLine("\nAntwort: ");
            Console.WriteLine("- Der Rückgabewert gibt ein wert eines bestimmten datentyps zu der caller funktion zurück.");

            Console.WriteLine("\n\nFrage 4:");
            Console.WriteLine("Wie viele Rückgabewerte kann eine Funktion haben?");

            Console.WriteLine("\nAntwort: ");
            Console.WriteLine("- Einen Rückgabewert (dieser kann aber auch ein Array von Daten sein)");

            Console.WriteLine("\n\nFrage 5:");
            Console.WriteLine("Warum sind Funktionen sinvoll?");

            Console.WriteLine("\nAntwort: ");
            Console.WriteLine("- Sie sind wiederverwendbar und strukturieren den Code");

        }
    }
}
