﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console_Interface
{
    class PDF1b
    {
        private static short menuInput;

        private static bool parsed;

        private static string[] MenuPointsPDF = {
            "Zurück",
            "Aufgabe a) C# Zum Ersten",
            "Aufgabe b) C# Zum Zweiten",
            "Aufgabe c) C# Zum Dritten",
            "Aufgabe d) C# Zum Vierten"
        };

        public static void Menu()
        {
            do
            {
                Console.Clear();
                AsciiArt.FileTitle("PDF 1b - Menü");
                AsciiArt.Menu(MenuPointsPDF);
                menuInput = AsciiArt.MenuInput(parsed, MenuPointsPDF);

                switch (menuInput)
                {
                    case 1:
                        ZumErsten();
                        AsciiArt.EndOfCodeWarning();
                        break;
                    case 2:
                        ZumZweiten();
                        AsciiArt.EndOfCodeWarning();
                        break;
                    case 3:
                        ZumDritten();
                        AsciiArt.EndOfCodeWarning();
                        break;
                    case 4:
                        ZumVierten();
                        AsciiArt.EndOfCodeWarning();
                        break;
                    case 0:
                        return;
                    default: Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Error: unable to use Menu-Switch!"); Console.ForegroundColor = ConsoleColor.White; break;

                }
            } while (menuInput != 0);

            menuInput = -1;
            return;
        }



        private static void ZumErsten()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - C# Zum Ersten");

            Console.WriteLine("\n\n\nFrage 1:");
            Console.WriteLine("Worin unterscheidet sich die Ausgabe von Console.Write( ) und Console.WriteLine( )?");

            Console.WriteLine("\nFrage 2:");
            Console.WriteLine("Welche Funktion hat die Methode Console.ReadKey( )?");

            Console.WriteLine("\n\nBeispiel:");
            Console.WriteLine("<> Mein erstes Programm");
            Console.Write("<> testen ? ");
            string assigned = Convert.ToString(Console.ReadLine());
            if (assigned == "y" || assigned == "Y" || assigned == "j" || assigned == "J")
                BeispielZumErsten();

            ZumErstenLösung();
        }

        private static void BeispielZumErsten()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - Beispiel > C# Zum Ersten");

            //Mein erstes Programm.            // 1. Teil:            
            Console.WriteLine("\n\nWerner-von-Siemens-Schule");
            Console.WriteLine("=========================\n\n\n");

            // 2. Teil:            
            string Schule = "Werner-von-Siemens-Schule";
            string Klasse = "10FIXY";
            Console.Write(Schule);
            Console.WriteLine(", Klasse: " + Klasse + "\n\n");

            // 3. Teil:            
            string Name;
            Console.Write("Name: ");
            Name = Console.ReadLine();
            Console.WriteLine("Mein Name ist: " + Name);

            // 4. Teil:            
            Console.Write("\n\n\nMein Name ist " + Name + ", ich bin Schüler der Klasse " + Klasse);
            Console.WriteLine(" an der " + Schule + ".");

            AsciiArt.EndOfCodeWarning();
        }

        private static void ZumErstenLösung()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - Lösung > C# Zum Ersten");

            Console.WriteLine("\n\n\nFrage 1:");
            Console.WriteLine("Worin unterscheidet sich die Ausgabe von Console.Write( ) und Console.WriteLine( )?");

            Console.WriteLine("\nAntwort: ");
            Console.WriteLine("- Console WriteLine schreibt eine komplette Zeile");
            Console.WriteLine("- Console Write schreibt in eine Zeile hinein");


            Console.WriteLine("\n\nFrage 2:");
            Console.WriteLine("Welche Funktion hat die Methode Console.ReadKey( )?");

            Console.WriteLine("\nAntwort: ");
            Console.WriteLine("- Console ReadKey stoppt die Befehlsverarbeitung solange, bis ein (utf-8)charakter gelesen wird.");
        }



        private static void ZumZweiten()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - C# Zum Zweiten");

            Console.WriteLine("\n\n\nFrage 1:");
            Console.WriteLine("Was passiert, wenn Sie für Länge, Breite und Höhe Zahlenwerte mit Nachkommastellen eingeben?");

            Console.WriteLine("\nFrage 2:");
            Console.WriteLine("Welchen Zweck erfüllt die Methode Convert.ToInt32(...)?");

            Console.WriteLine("\n\nBeispiel:");
            Console.WriteLine("<> Berechnung eines Quadervolumens");
            Console.Write("<> testen ? ");
            string assigned = Convert.ToString(Console.ReadLine());
            if (assigned == "y" || assigned == "Y" || assigned == "j" || assigned == "J")
                BeispielZumZweiten();

            ZumZweitenLösung();
        }

        private static void BeispielZumZweiten()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - Beispiel > C# Zum Zweiten");


            // Berechnung eines Qauadervolumens            
            int l, b, h, v;
            Console.WriteLine("\n\nBerechnung eines Quadervolumens:\n");
            string ls;

            Console.Write("Länge l= ");
            ls = Console.ReadLine();
            l = Convert.ToInt32(ls);

            Console.Write("Breite b= ");
            b = Convert.ToInt32(Console.ReadLine());

            Console.Write("Höhe h= ");
            h = Convert.ToInt32(Console.ReadLine());

            v = l * b * h;

            Console.WriteLine("Das Quadervolumen  v= " + v);       

            AsciiArt.EndOfCodeWarning();
        }

        private static void ZumZweitenLösung()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - Lösung > C# Zum Zweiten");

            Console.WriteLine("\n\n\nFrage 1:");
            Console.WriteLine("Was passiert, wenn Sie für Länge, Breite und Höhe Zahlenwerte mit Nachkommastellen eingeben?");

            Console.WriteLine("\nAntwort: ");
            Console.WriteLine("- Die Fehlermeldung: System.FormatException: 'Input string was not in a correct format.', erscheint. ");

            Console.WriteLine("\n\nFrage 2:");
            Console.WriteLine("Welchen Zweck erfüllt die Methode Convert.ToInt32(...)?");

            Console.WriteLine("\nAntwort: ");
            Console.WriteLine("- Sie konvertiert einen beliebigen Datentyp zu einem Integer.");
            
        }

        private static void ZumDritten()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - C# Zum Dritten");

            Console.WriteLine("\n\n\nFrage 1:");
            Console.WriteLine("Worin unterscheidet sich die Ergebnisausgabe (vorletzte Zeile), von der im vorigen Programm?");

            Console.WriteLine("\n\nBeispiel:");
            Console.WriteLine("<> Berechnung eines Quadervolumens");
            Console.Write("<> testen ? ");
            string assigned = Convert.ToString(Console.ReadLine());
            if (assigned == "y" || assigned == "Y" || assigned == "j" || assigned == "J")
                BeispielZumDritten();

            ZumDrittenLösung();
        }

        private static void BeispielZumDritten()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - Beispiel > C# Zum Dritten");


            // Berechnung eines Qauadervolumens            
            int l, b, h, v;
            Console.WriteLine("\n\nBerechnung eines Quadervolumens:\n");
            string ls;

            Console.Write("Länge l= ");
            ls = Console.ReadLine();
            l = Convert.ToInt32(ls);

            Console.Write("Breite b= ");
            b = Convert.ToInt32(Console.ReadLine());

            Console.Write("Höhe h= ");
            h = Convert.ToInt32(Console.ReadLine());

            v = l * b * h;

            Console.WriteLine("Das Quadervolumen  v= {0}", v); //Formatierung

            AsciiArt.EndOfCodeWarning();
        }

        private static void ZumDrittenLösung()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - Lösung > C# Zum Dritten");

            Console.WriteLine("\n\n\nFrage 1:");
            Console.WriteLine("Worin unterscheidet sich die Ergebnisausgabe (vorletzte Zeile), von der im vorigen Programm?");

            Console.WriteLine("\nAntwort: ");
            Console.WriteLine("- Die Ausgabe der variable v wird hier Formatiert ausgegeben, im vorigen Beipiel nicht.");

        }



        private static void ZumVierten()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - C# Zum Vierten");

            Console.WriteLine("\n\n\nFrage 1:");
            Console.WriteLine("Welchen Fehler verursacht das Programm?");

            Console.WriteLine("\n\nBeispiel:");
            Console.WriteLine("<> Berechnung eines Kugelvolumens ");
            Console.Write("<> testen ? ");
            string assigned = Convert.ToString(Console.ReadLine());
            if (assigned == "y" || assigned == "Y" || assigned == "j" || assigned == "J")
                BeispielZumVierten();

            ZumViertenLösung();
        }

        private static void BeispielZumVierten()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - Beispiel > C# Zum Vierten");


            // Berechnung eines Kugelvolumens            
            double r, vIntOP, vDoubleOP;
            Console.WriteLine("\n\nBerechnung eines Kugelvolumens:\n");
            Console.Write("Radius r= ");
            r = Convert.ToDouble(Console.ReadLine());
            vIntOP = 4 / 3 * Math.PI * r*r*r;  // IntegerDivision !!!        
            vDoubleOP = 4.0 / 3.0 * Math.PI * r * r * r;  // FloatingPointNumberDivision !!!             
            Console.WriteLine("Das falsche Kugelvolumen  v= 4/3*{0}*{1}³= {2}",Math.PI,r, vIntOP);
            Console.WriteLine("Das richtige Kugelvolumen  v= 4.0/3.0*{0}*{1}³= {2}", Math.PI, r, vDoubleOP);

            AsciiArt.EndOfCodeWarning();
        }

        private static void ZumViertenLösung()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1b - Lösung > C# Zum Vierten");

            Console.WriteLine("\n\n\nFrage 1:");
            Console.WriteLine("Welchen Fehler verursacht das Programm?");

            Console.WriteLine("\nAntwort: ");
            Console.WriteLine("- Durch die Division mit Integer anstatt Double, wird das Ergebnis falsch berechnet");

        }
    }
}
