﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Console_Interface
{
    class Program
    {
        private static short menuInput;

        private static bool parsed;

        private static string[] MenuPointsHauptmenü = {
            "Beenden",
            "Aufgaben"
        };

        private static string[] MenuPointsAufgaben = {
            "Zurück",
            "PDF 1      Variablen",
            "PDF 1b     Erste C# Programme",
            "PDF 2      BMI Rechner und Funktionen",
            "PDF 3      Rabattrechnung",
            "PDF 4",
            "PDF 5",
            "PDF 6",
            "PDF 7"
        };
        [STAThread]
        static void Main()
        {
            Hauptmenü();
            AsciiArt.EndOfCodeWarning();
        }

        private static void Hauptmenü()
        {
            do
            {
                Console.Clear();
                AsciiArt.FileTitle("Lernfeld 6 - Hauptmenü");
                AsciiArt.Menu(MenuPointsHauptmenü);
                menuInput = AsciiArt.MenuInput(parsed, MenuPointsHauptmenü);


                switch (menuInput)
                {
                    case 1:
                        Aufgabenmenü();
                        break;
                    case 0:

                        break;
                    default: Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Error: unable to use Menu-Switch!"); Console.ForegroundColor = ConsoleColor.White; break;
                }

            } while (menuInput != 0) ;
        }




        private static void Aufgabenmenü()
        {
            do
            {

                Console.Clear();
            AsciiArt.FileTitle("Lernfeld 6 - Aufgaben");
            AsciiArt.Menu(MenuPointsAufgaben);
            menuInput = AsciiArt.MenuInput(parsed, MenuPointsAufgaben);


            switch (menuInput)
            {
                case 1:
                    PDF1.Menu();
                    break;
                case 2:
                    PDF1b.Menu();
                    break;
                case 3:
                    PDF2.Menu();
                    break;
                case 4:
                    PDF3.RabattRechnung();
                    break;
                case 5:
                    PDF4.Menu();
                    AsciiArt.EndOfCodeWarning();
                    break;
                case 6:
                    PDF5.Menu();
                    AsciiArt.EndOfCodeWarning();
                    break;
                case 7:
                    PDF6.Menu();
                    AsciiArt.EndOfCodeWarning();
                    break;
                case 8:
                    PDF7.Menu();
                    AsciiArt.EndOfCodeWarning();
                    break;
                case 0:

                    break;
                default: Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Error: unable to use Menu-Switch!"); Console.ForegroundColor = ConsoleColor.White; break;

            }
            } while (menuInput != 0);

            if (menuInput != 0) AsciiArt.EndOfCodeWarning();
            menuInput = -1;
            return;
        }


    }
}
