﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console_Interface
{
    class PDF3
    {
        public static void RabattRechnung()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 3 - Anwendung > Rabattrechnung ");

            string rabatt = "keiner";
            double neuergesamtpreis = 0;
            bool neuerArtikel = false;
            do
            {

                Console.Write("\nEinzelpreis: ");
                double einzelpreis = Convert.ToDouble(Console.ReadLine());

                Console.Write("\nVerkaufte Menge: ");
                double verkaufteMenge = Convert.ToDouble(Console.ReadLine());

                double gesamtpreis = einzelpreis * verkaufteMenge;



                if (gesamtpreis > 99 && gesamtpreis < 500)
                {
                    neuergesamtpreis += gesamtpreis * 0.95;
                    rabatt = "5%";
                }
                else if (gesamtpreis > 499)
                {
                    neuergesamtpreis += gesamtpreis * 0.93;
                    rabatt = "7%";
                }
                else
                {
                    rabatt = "0%";
                    neuergesamtpreis += gesamtpreis;
                }
                Console.Write("Neuer Artikel ? ");
                string input = Console.ReadLine();
                if (input == "y" || input == "Y" || input == "j" || input == "J") neuerArtikel = true;
                else neuerArtikel = false;

            } while (neuerArtikel == true);


            Console.Write("\nRabatt: " + rabatt);
            Console.Write("\nGesamtpreis: " + neuergesamtpreis);


            AsciiArt.EndOfCodeWarning();
        }
    }
}
