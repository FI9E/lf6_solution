﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console_Interface
{
    class AsciiArt
    {


        public static void FileTitle(string content)
        {
            string adjustedTitle = "| " + content + " |";
            int characterLength = adjustedTitle.Length;

            Console.WriteLine(adjustedTitle);

            Console.Write("|");
            for (int columnpointer = 0; columnpointer < characterLength - 2; columnpointer++)
                Console.Write("_");
            Console.Write("|");

            return;
        }


        public static void Menu(string[] points)
        {
            Console.WriteLine("\n\nConsole Menu");
            for(int point=0; point<points.Length; point++)
            {
                Console.WriteLine("["+point+"] " + points[point]);
            }
        }


        public static short MenuInput(bool parsed, string[] MenuPoints)
        {
            short menuSelection = -1;
            do
            {
                Console.Write("\nAuswahl: ");
                string input = Console.ReadLine();
                int min = 0;
                int max = MenuPoints.Length - 1;

                parsed = Int16.TryParse(input, out short selection);
                if (!parsed || selection < min || selection > max)
                {

                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error: Auswahl ist keine ganze Zahl zwischen "+ min +" und "+ max + "!");
                    Console.ForegroundColor = ConsoleColor.White;

                    parsed = false;
                }

                else
                {
                    menuSelection = selection;
                }
            } while (!parsed);

            return menuSelection;
        }


        //Display "end of code" Message
        public static void EndOfCodeWarning()
        {
            Console.WriteLine("\n\n\n");
            for (int columnpointer = 0; columnpointer < 18; columnpointer++)
                Console.Write("<>");

            Console.WriteLine("\n>          End of Code!            <\n" +
                                "<    Press any key to exit. . .    >");

            for (int columnpointer = 0; columnpointer < 18; columnpointer++)
                Console.Write("><" +
                    "");

            Console.ReadKey();
        }
    }
}
