﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console_Interface
{
    class PDF1
    {
        private static short menuInput;

        private static bool parsed;

        private static string[] MenuPointsPDF = {
            "Zurück",
            "Aufgabe a) Höchster Wert",
            "Aufgabe b) Höchster Wert +1",
            "Aufgabe c) Höchster Wert Rechnung"
        };

        public static void Menu()
        {
            do
            {
                Console.Clear();
                AsciiArt.FileTitle("PDF 1 - Menü");
                AsciiArt.Menu(MenuPointsPDF);
                menuInput = AsciiArt.MenuInput(parsed, MenuPointsPDF);

                switch (menuInput)
                {
                    case 1:
                        HöchsterWert();
                        AsciiArt.EndOfCodeWarning();
                        break;
                    case 2:
                        HöchsterWertPlusEins();
                        AsciiArt.EndOfCodeWarning();
                        break;
                    case 3:
                        HöchsterWertRechnung();
                        AsciiArt.EndOfCodeWarning();
                        break;
                    case 0:
                        return;
                    default: Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Error: unable to use Menu-Switch!"); Console.ForegroundColor = ConsoleColor.White; break;

                }
            } while (menuInput != 0);

            menuInput = -1;
            return;
        }


        private static void HöchsterWert()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1 - Höchster Wert");

            Console.WriteLine("\n\n\nAufgabe: ");
            Console.WriteLine("Weisen Sie einer Variable den höchsten Wert zu und geben sie ihn aus.");

            int maxValInt32Function = Int32.MaxValue;
            int maxValInt32 = 2147483647;

            Console.WriteLine("\n\nLösung: ");
            Console.WriteLine("Zuweisung durch Int32.MaxValue  :   "+ maxValInt32Function);
            Console.WriteLine("Zuweisung Hardcoded im Quellcode:   "+ maxValInt32);

            Console.WriteLine("\n\nFazit: ");
            Console.WriteLine("Ausgabe gleicht Zugewiesenen Wert.");
        }

        private static void HöchsterWertPlusEins()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1 - Höchster Wert Plus Eins");


            Console.WriteLine("\n\n\nAufgabe: ");
            Console.WriteLine("Weisen Sie einer Variable den höchsten Wert + 1 zu und geben sie ihn aus.");


            Console.WriteLine("\n\nLösung: ");
            int maxValInt32Function = Int32.MaxValue;
            maxValInt32Function += 1;
            Console.WriteLine("Positive MaxValue + 1 :   " + maxValInt32Function);
            maxValInt32Function -= 1;
            Console.WriteLine("Negative MaxValue - 1 :   " + maxValInt32Function);

            Console.WriteLine("\n\nFazit: ");
            Console.WriteLine("Bei überschreitung des positiven amximalwerts um +1, wechselt er zum negativen maximalwert.\n" +
                              "Bei überschreitung des negativen maximalwerts um -1, wechselt er zum positiven maximalwert.");
        }

        private static void HöchsterWertRechnung()
        {
            Console.Clear();
            AsciiArt.FileTitle("PDF 1 - Höchster Wert Rechnung");

            Console.WriteLine("\n\n\nAufgabe: ");
            Console.WriteLine("Weisen Sie einer Variable einen zulässigen Wert zu und verändern sie edn Wert \n" +
                              "durch eine Recheenoperation derart, dass der zulässige Wertebereich überschritten wird.");

            Console.WriteLine("\n\nLösung: ");
            int maxValInt32Function = Int32.MaxValue;
            Console.WriteLine("Rechenoperation: Wert = Wert + 40");
            Console.WriteLine("Wert vor Rechenoperation :   " + maxValInt32Function);
            maxValInt32Function += 40;
            Console.WriteLine("Wert nach Rechenoperation:   " + maxValInt32Function);

            Console.WriteLine("\n\nFazit: ");
            Console.WriteLine("Die Rechnung geht im gegenübergestzten Wertebereich, jedoch in der selben Richtung wie davor, weiter ");
        }
    }
}
