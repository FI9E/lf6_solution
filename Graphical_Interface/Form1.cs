﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Graphical_Interface
{
    public partial class Form1 : Form
    {
        private static double cc = 100;


        public Form1()
        {
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            InitializeComponent();
        }


        private void BackToConsole(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics g = e.Graphics;
            Rectangle r = this.ClientRectangle;

            using (Pen pen = new Pen(Color.Black))
            {


                ScaleTransformFloatMatrixOrder(e);
            }


        }

        private void ScaleTransformFloatMatrixOrder(PaintEventArgs e)
        {



            // Draw rotated, scaled rectangle to screen.
            e.Graphics.DrawLine(new Pen(Color.LightGray), 0, 0, 200, 0);
            for(int i=0; i<200; i=i+20)
                if(i != 100) e.Graphics.DrawLine(new Pen(Color.LightGray), 0, i, 200, i);
                else if(i == 100) e.Graphics.DrawLine(new Pen(Color.Blue), 0, i, 200, i);

            // Draw rotated, scaled rectangle to screen.
            e.Graphics.DrawLine(new Pen(Color.Gray), 0, 0, 0, 200);
            for (int i = 0; i < 200; i = i + 20)
                if(i != 100) e.Graphics.DrawLine(new Pen(Color.Gray), i, 0, i, 200);
                else if(i == 100) e.Graphics.DrawLine(new Pen(Color.Red), i, 0, i, 200);

 // Create pens.
    Pen redPen = new Pen(Color.Red, 3);
    Pen greenPen = new Pen(Color.Green, 3);

            // Create points that define curve.

            Point point0 = new Point(50 - 20, (int)cc - 180);
            Point point1 = new Point(60-20, (int)cc-80);

    Point point2 = new Point(90 - 20, (int)cc);

    Point point3 = new Point(110 - 20, (int)cc);


    Point point4 = new Point(140 - 20, (int)cc -80);

    Point point5 = new Point(150 - 20, (int)cc - 180);

            Point[] curvePoints = {point0, point1, point2, point3, point4, point5};

            Debug.WriteLine("drawing");


    // Draw curve to screen.
    e.Graphics.DrawCurve(greenPen, curvePoints);        }

        private void berechneNullstellen(object sender, EventArgs e)
        {
            panel2.BackColor = SystemColors.ActiveBorder;

            double x1 = 0.0, x2 = 0.0, a, b, c, D;

            a = Convert.ToDouble(textBox1.Text);
            b = Convert.ToDouble(textBox2.Text);
            c = Convert.ToDouble(textBox3.Text);

            D = (Math.Sqrt(b * b - 4 * a * c));

            if (D > 0)
            {
                x1 = (-b - (Math.Sqrt(b * b - 4 * a * c))) / (2 * a);
                x2 = (-b + (Math.Sqrt(b * b - 4 * a * c))) / (2 * a);

                Console.WriteLine("Lösung 1 = {0:F2}", x1);
                Console.WriteLine("Lösung 2 = {0:F2}", x2);
            }
            else if (D == 0)
            {
                x1 = (-b / (2 * a));
                Console.WriteLine("Die Lösung = {0:F2}", x1);
            }
            else
            {
                Console.WriteLine("Lösung 1 = keine Lösung!");
                Console.WriteLine("Lösung 2 = keine Lösung!");
                panel2.BackColor = Color.Red;
            }
            

            switch(c)
            {
                case 4: cc = 20.0; break;
                case 3: cc = 40.0; break;
                case 2: cc = 60.0; break;
                case 1: cc = 80.0; break;
                case 0: cc = 100.0; break;
                case -1: cc = 120.0; break;
                case -2: cc = 140.0; break;
                case -3: cc = 160.0; break;
                case -4: cc = 180.0; break;
            }

            label5.Text = "x1 = "+x2;
            label6.Text = "x2 = " + x1;

            panel1.Refresh();
        }
    }
}
